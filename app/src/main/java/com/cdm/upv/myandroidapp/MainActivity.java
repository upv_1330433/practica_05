package com.cdm.upv.myandroidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.view.View.OnClickListener;
class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button button;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = (ImageView) findViewById(R.id.imageView1);
        findViewById(R.id.btnSandia).setOnClickListener( this);
        findViewById(R.id.btnManzana).setOnClickListener(this);
        findViewById(R.id.btnPina).setOnClickListener(this);
        findViewById(R.id.btnUva).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnManzana:
                image.setImageResource(R.drawable.manzana); //nombre del archivo imagen
                break;
            case R.id.btnPina:
                image.setImageResource(R.drawable.pina); //nombre del archivo imagen
                break;
            case R.id.btnUva:
                image.setImageResource(R.drawable.uva); //nombre del archivo imagen
                break;
            case R.id.btnSandia:
                image.setImageResource(R.drawable.sandia); //nombre del archivo imagen
                break;
        }
    }
}
